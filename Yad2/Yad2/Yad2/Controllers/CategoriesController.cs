﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Yad2.Controllers
{
    [RoutePrefix("api/Categories")]
    public class CategoriesController : ApiController
    {
        [HttpGet]
        [Route("getAll")]
        public IHttpActionResult getAll()
        {
            return Ok(categoryBLL.GetAllcategory());
        }


    }
}
