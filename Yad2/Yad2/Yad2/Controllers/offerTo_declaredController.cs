﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BLL;
using DAL;

namespace Yad2.Controllers
{
    [RoutePrefix("api/offerTo_declare")]
    public class offerTo_declaredController : ApiController
    {
        [Route("get")]
        public IHttpActionResult getAll()
        {
            return Ok(offerTo_declaredBLL.gettAll());
        }
        [HttpPost]
        [Route("offerTo_declared")]
        public IHttpActionResult add([FromBody] offerTo_declared o)
        {
            offerTo_declaredBLL.addfferTo_declared(o);

            return Ok(true);
        }

    }
}
