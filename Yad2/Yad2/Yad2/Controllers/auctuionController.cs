﻿using BLL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Yad2.Controllers
{
    [RoutePrefix("api/auctuion")]
    public class auctuionController : ApiController
    {
        //שליפת המכרזים
        [HttpGet]
        [Route("getAllMechrazim")]
        public List<auctuion> getAllMechrazim()
        {
            return auctuionBLL.GetActiveAuctuions();
        }
        [HttpGet]
        [Route("login")]
        public people login(string mail, string password)
        {
            return auctuionBLL.login( mail,  password);
        }
        //שליפה לפי  קוד
        public IHttpActionResult get(int id_category)
        {
            return Ok(auctuionBLL.get(id_category));
        }
        [Route("search/{from}/{to}")]
        [HttpPost]
        ///חיפוש מכרזים פעילים
        public IHttpActionResult search([FromBody]int[] arridCategory, int from, int to)
        {
            return Ok(auctuionBLL.search(from, to, arridCategory));
        }
        [Route("addUser")]
        [HttpPost]

        ///חיפוש מכרזים פעילים
        public bool addUser([FromBody] people user)
        {
            return auctuionBLL.addUser(user);
        }


        [Route("addAuctuion")]
        [HttpPost]
        public IHttpActionResult addAuctuion([FromBody] auctuion auctuion)
        {
            auctuion.city = "ירושלים";
            auctuion.color = "טבעי";
            auctuion.degem = "א";
            auctuion.ProductionYear = 2020;
            auctuion.size = "גדול";
            auctuion.yatzran = "ישראל";
            auctuion.status = "סטטוס";
            return Ok(auctuionBLL.addAuctuion(auctuion));
        }

        [Route("getCurrentUsersAuctuions/{id}")]
        [HttpGet]
        public IHttpActionResult getCurrentUsersAuctuions(int id)
        {
            return Ok(auctuionBLL.getCurrentUsersAuctuions(id));
        }

        //[Route("getBidsToAucuions")]
        //[HttpPost]
        //public IHttpActionResult GetBidsToAucuions([FromBody] auctuions)
        //{
        //    reutrn Ok()
        //}


    }
}
