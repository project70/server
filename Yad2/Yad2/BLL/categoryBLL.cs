﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class categoryBLL
    {
        static LocationEntities1 db = new LocationEntities1();
        //שליפה
        public static List<category> GetAllcategory()
        {

            return db.category.ToList();
        }
        //הוספה
        public static void category(category a)
        {
            LocationEntities1 db = new LocationEntities1();
            db.category.Add(a);
            db.SaveChanges();
        }
        //פונקציה שמקבלת קוד קטגוריה ושולפת את כל הפרמטרים הקשורים לה
        public static List<parameterTo_category> get_parameter (string id_category)
        {
            return db.parameterTo_category.Where(h => h.id_parameter == id_category).ToList();
        }
    }
}
