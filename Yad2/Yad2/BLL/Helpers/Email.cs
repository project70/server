﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace BLL.Helpers
{
    class Email
    {  //פונקציה זו מקבלת רשימת נמענים לשליחה 
        //מקבלת נושא אימייל ותוכן
        //=לא חובה וקבצים מצורפים לשליחה
        //מחזיר true אם הצליח 
        //מחזיר false אם לא הצליח
        //אם התצוגה של ההודעה תיהיה HTML showHtml מקבל 

        public static bool SendEmail(string[] to, string subject, string content, bool showHtml = false, string[] attachmentFilename = null)

        {
            SmtpClient smtpClient = new SmtpClient();
            var basicCredential = new NetworkCredential("rt202114@gmail.com", "447672122");
            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress("rt202114@gmail.com");

            smtpClient.Host = "smtp.gmail.com";
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = basicCredential;
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;


            message.From = fromAddress;
            message.Subject = subject;

            message.Body = content;


            //אם קיימים קבצים לשליחה
            if (attachmentFilename != null)
            {
                //עובר על מערך הקבצים ומוסיף אותם להודעה
                foreach (string file in attachmentFilename)
                {
                    message.Attachments.Add(new Attachment(file));
                }
            }
            // HTMLאם תצוגת ההודעה תיהיה עי  
            if (showHtml == true)
            {
                message.IsBodyHtml = true;
            }


            //עובר על רשימת הנמענים
            foreach (var item in to)
            {
                message.To.Add(item);
            }

            try
            {
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                //Error, could not send the message
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
    }
}
