﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Configuration;

namespace BLL
{
    public class auctuionBLL
    {
      static  LocationEntities1 db = new LocationEntities1();
        public static List<auctuion> getAll()
        {
            return db.auctuion.ToList();
        }

        public static List<auctuion>  GetActiveAuctuions()
        {
            List<auctuion> list = db.auctuion.Include("offerTo_declared")
                .Where(a => a.date_close >= DateTime.Today)
                .OrderByDescending(a => a.offerTo_declared.Count).Take(6).ToList();
            return list;
        }

        public static people login(string mail, string password)
        {
            people a = db.people.FirstOrDefault(x=> x.password == password && x.gmail==mail);
            return a;
        }

        //פונקציה שמקבלת קוד קטגוריה ושולפת את כל המכרזים שקשורים אליה
        public static List<auctuion> get(int id_category)
        {
            return db.auctuion.Where(k => k.id_category == id_category).ToList();
        }
        //הוספה
        public static void auctuion(auctuion a)
        {
            LocationEntities1 db = new LocationEntities1();
            db.auctuion.Add(a);
            db.SaveChanges();
        }

        public static bool addUser(people user)
        {
            try
            {
                LocationEntities1 db = new LocationEntities1();
                db.people.Add(user);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
           
        }

        //עידכון
        public static void updateauctuion(auctuion a)
        {
            LocationEntities1 db = new LocationEntities1();
            auctuion u = db.auctuion.FirstOrDefault(o => o.id_auction == a.id_auction);
            u = a;
            db.SaveChanges();
        }
        //מחיקה
        public static void delateauctuion(auctuion a)
        {
            LocationEntities1 db = new LocationEntities1();
            db.auctuion.Remove(a);
            db.SaveChanges();
        }
        //חיפוש
        public static List<auctuion> search(int from,int to, int[] arridCategory)
        {
          
            List<category> lc = db.category.ToList();
            List<auctuion> la =new List<auctuion>();

            var hasCategories = arridCategory.Length > 0;
            la = db.auctuion.Where(a => a.date_close >= DateTime.Today
                && (hasCategories ? arridCategory.Contains(a.id_category ?? 0) : true)
                && (from > 0 ? a.price_start >= from : true) 
                && (to > 0 ? a.price_start <= to : true)).ToList();

            la.ForEach(item => item.category = null);

            return la;
        }


        public static List<auctuion> addAuctuion(auctuion auctuion)
        {
            LocationEntities1 db = new LocationEntities1();
            db.auctuion.Add(auctuion);
            db.SaveChanges();
            string message;
            message =
                "<h1>פרטי המכרז שלך:</h1><br><span> "
            + auctuion.product_des + "<br>" + auctuion.ProductionYear + "<br>" + auctuion.price_start + "<br>"
            + auctuion.yatzran + "<br>" + auctuion.date_publication + "<br>" + auctuion.date_close;
            string[] address = new string[]
            {
                db.people.First(p => p.id == auctuion.id_person).gmail
    };
            Email.SendEmail(address, "העלאת מכרז ",
                   message, true, null);
            return db.auctuion.ToList();

        }

        public static List<auctuion> getCurrentUsersAuctuions(int id)
        {
            return db.auctuion.Select(a => a).Where(a1 => a1.id_person == id).ToList();
        }


        //public static List<offerTo_declared> GetBidsToAucuions(List<auctuion> auctuions)
        //{
        //    List<offerTo_declared> offers = new List<offerTo_declared>();
        //    auctuions.ForEach(a=> db.offerTo_declared.Select(o=> o.id_declared))
        //}

    }
}
