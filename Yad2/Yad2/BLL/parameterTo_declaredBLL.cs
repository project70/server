﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
namespace BLL
{
    public class parameterTo_declaredBLL
    {
        static LocationEntities1 db = new LocationEntities1();
        //הוספה
        public static void parameterTo_declared(parameterTo_declared a)
        {
            LocationEntities1 db = new LocationEntities1();
            db.parameterTo_declared.Add(a);
            db.SaveChanges();
        }

        //עידכון
        public static void updateparameterTo_declared(parameterTo_declared a)
        {
            LocationEntities1 db = new LocationEntities1();
            parameterTo_declared u = db.parameterTo_declared.FirstOrDefault(o => o.id_parameterTo_declared == a.id_parameterTo_declared);
            u = a;
            db.SaveChanges();
        }
        //מחיקה
        public static void delateparameterTo_declared(parameterTo_declared a)
        {
            LocationEntities1 db = new LocationEntities1();
            db.parameterTo_declared.Remove(a);
            db.SaveChanges();
        }

        //פונקציה שמקבלת קוד מכרז ושולפת את כל המכרזים שקשורים אליה
        public static List<auctuion> get_action(int id_action)
        {
            return db.auctuion.Where(k => k.id_auction == id_action).ToList();
        }
    }
}
