﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
namespace BLL
{
    public class parameterTo_categoryBLL
    {
        //שליפת כל העצעות
        public static List<parameterTo_category>gettAll()
        {
            LocationEntities1 db = new LocationEntities1();
            return db.parameterTo_category.ToList();
        }

        //הוספה
        public static void addparameterTo_category(parameterTo_category a)
        {
            LocationEntities1 db = new LocationEntities1();
            db.parameterTo_category.Add(a);
            //db.SaveChanges();
        }
        //עידכון
        public static void updateparameterTo_category(parameterTo_category a)
        {
            LocationEntities1 db = new LocationEntities1();
            parameterTo_category u = db.parameterTo_category.FirstOrDefault(o => o.id_parameter == a.id_parameter);
            u = a;
            db.SaveChanges();
        }
    }
}
