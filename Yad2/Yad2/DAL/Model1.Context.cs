﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class LocationEntities1 : DbContext
    {
        public LocationEntities1()
            : base("name=LocationEntities1")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<auctuion> auctuion { get; set; }
        public virtual DbSet<category> category { get; set; }
        public virtual DbSet<offerTo_declared> offerTo_declared { get; set; }
        public virtual DbSet<parameterTo_category> parameterTo_category { get; set; }
        public virtual DbSet<parameterTo_declared> parameterTo_declared { get; set; }
        public virtual DbSet<people> people { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
    }
}
