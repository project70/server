//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class auctuion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public auctuion()
        {
            this.offerTo_declared = new HashSet<offerTo_declared>();
            this.parameterTo_declared = new HashSet<parameterTo_declared>();
        }
    
        public int id_auction { get; set; }
        public Nullable<int> id_person { get; set; }
        public Nullable<int> id_category { get; set; }
        public string product_des { get; set; }
        public Nullable<System.DateTime> date_publication { get; set; }
        public Nullable<int> price_start { get; set; }
        public Nullable<System.DateTime> date_close { get; set; }
        public string image { get; set; }
        public string color { get; set; }
        public string size { get; set; }
        public string city { get; set; }
        public Nullable<int> ProductionYear { get; set; }
        public string yatzran { get; set; }
        public string degem { get; set; }
        public string status { get; set; }
    
        public virtual category category { get; set; }
        public virtual people people { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<offerTo_declared> offerTo_declared { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<parameterTo_declared> parameterTo_declared { get; set; }
    }
}
